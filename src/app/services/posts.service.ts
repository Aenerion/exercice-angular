import { Injectable } from '@angular/core';
import { Post } from '../models/post.model';
import { Subject } from 'rxjs';

@Injectable()
export class PostsService {


  posts: Post[] = [
    new Post(0, 'Itinera sunt conducentia nequo Gallus.',
     'Elegerit id in illis marito sexus species apud id si dotis coniunx marito apud sit marito id et elegerit ut et in pacto ex offert apud fuga incredibile nomine ad.',
      0,
      '2012-08-25T18:25:43.511Z'),
      new Post(1, 'Scaevola ut C sermonem tum.',
      'Quaedam iunxerat rumigerulos falsa iunxerat Constantinus Constantinus clandestinos cruoris fratris accesserat artium mitius Hannibaliano quaedam antehac Constantinus adsidua rumigerulos avida clandestinos grave maritus quam qui processu per acerbitati turgida antehac processu grave discentes antehac temporis quam Constantinus male cruoris nihil avida Augusti versutosque inflammatrix nocendum fratris modum modum incentivum Constantinus.',
       5,
       '2016-04-02T15:25:43.511Z'),
       new Post(2, 'Has sed navem sed litoribus.',
       'Mora horreis viri postea populo novimus tempore ex frumentum Carthaginiensibus pauloque destinatis actitatum pauloque iam cum negotium iam destinatis negotium negotium dedit est negotium Hymetii Carthaginiensibus Hymetii actitatum copia provenisset tempore cum lassatis lassatis copia consule destinatis provenisset etiam integre.',
      -3,
      '2017-10-21T20:25:43.511Z'),
  ];

  postSubject = new Subject<Post[]>();

addPost(title: string, content: string) {
  var id;
  try{
     id = this.posts[(this.posts.length - 1)].id + 1;
   }
  catch(e){
    id = 0;
   }
    var date = new Date();
    var postObject = new Post( id,title,content,0,date.toISOString());        
    this.posts.push(postObject);
  }


  removePost(id:number){
    console.log('delete ' + id );
    this.posts.forEach( (post, index) => {
       if(post.id === id) this.posts.splice(index,1);
      });
    }


    incrementLike(id:number){
      console.log('incrementLike ' + id );
      this.posts.forEach( (post, index) => {
         if(post.id === id) post.loveIts++;
        });
      }


    decrementLike(id:number){
      console.log('decrementLike ' + id );
      this.posts.forEach( (post, index) => {
         if(post.id === id) post.loveIts--;
        });
      }


    emitPosts()
    {
      this.postSubject.next(this.posts.slice())
    }

    
}