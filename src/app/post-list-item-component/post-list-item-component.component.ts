import { Component, Input, OnInit } from '@angular/core';
import { PostsService } from '../services/posts.service';
import { Post } from '../models/post.model';

@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.scss']
})
export class PostListItemComponentComponent implements OnInit {

  @Input() post : Post;
  
  constructor(private postsService: PostsService) { }
 
  ngOnInit() {
  }

  onLoveIt() {
    this.postsService.incrementLike(this.post.id);
    this.postsService.emitPosts();
}

onDontLoveIt() {
  this.postsService.decrementLike(this.post.id);
  this.postsService.emitPosts();
}

delete(){
  this.postsService.removePost(this.post.id);
  this.postsService.emitPosts();
}

}
